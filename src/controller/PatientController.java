package controller;
import java.io.IOException;
import java.sql.Date;
import java.util.List;
import dao.PatientImpl;
import model.Patient;
public class PatientController {
  Patient patient;
  PatientImpl impl = new PatientImpl();

  public String addPatient(int id, String name, int billpaid, long mobile, Date doj) {
    patient = new Patient(id, name, billpaid, mobile, doj); // Set the values to the POJO class /
                                                            // Assign the values to POJO class
    return impl.addPatient(patient);
  }

  public List<Patient> viewAll() {
    return impl.viewAll();
  }

  public String updatePatient(int id, String name, long mobile) {   
    patient = new Patient();                                         
    patient.setId(id);
    patient.setName(name);
    patient.setMobile(mobile);
    return impl.updatePatient(patient);
  }

  public String searchById(int id) {
    return impl.searchById(id);
  }

  public String deleteById(int id) {
    return impl.deleteById(id);
  }




public void addDoctor() {
     try {
		impl.addDoctor();
	} catch (ClassNotFoundException e) {
		
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	}
 }

public void viewDoctor() {
   
		impl.viewDoctor();
	

}

public void updateDoctor() {
   
		impl.updateDoctor();
		
		}

public void searchDoctor() {
   		impl.searchDoctor();
	
}

public void deleteDoctor() {
	impl.deleteDoctor();
}
}
