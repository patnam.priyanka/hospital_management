package model;
import java.sql.Date;
import java.text.SimpleDateFormat;
public class Patient {
  private int id;
  private String name;
  private int billpaid;
  private long mobile;
  private Date doj;

  public Patient() {
    // TODO Auto-generated constructor stub
  }

  public Patient(int id, String name, int billpaid, long mobile, Date doj) {
    super();
    this.id = id;
    this.name = name;
    this.billpaid = billpaid;
    this.mobile = mobile;
    this.doj = doj;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getBillpaid() {
    return billpaid;
  }

  public void setSalary(int billpaid) {
    this.billpaid = billpaid;
  }

  public long getMobile() {
    return mobile;
  }

  public void setMobile(long mobile) {
    this.mobile = mobile;
  }

  public Date getDoj() {
    return doj;
  }

  public void setDoj(Date doj) {
    this.doj = doj;
  }

  @Override
  public String toString() {
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMMM-yyyy EEEE"); //UI Format
    return String.format("%-10s%-20s%-10s%-15s%s", id, name, billpaid, mobile,
        dateFormat.format(doj));
  }

}