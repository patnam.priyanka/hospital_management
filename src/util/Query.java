package util;
public class Query {
	  public static final String ADD_PATIENT = "insert into patient values(?,?,?,?,?)";
	  public static final String VIEW_ALL = "select * from patient";
	  public static final String UPDATE = "update patient set name=?,mobile=? where id=?";
	  public static final String SEARCH_BY_ID = "select * from patient where id=?";
	  public static final String DELETE_BY_ID = "delete from patient where id=?";
	  
	  
	  public static final String ADD_DOCTOR = "insert into doctor values(?,?,?,?)";
	  public static final String VIEW_DOCTOR = "select * from doctor";
	  public static final String UPDATE_DOCTOR = "update doctor set name=?,mobile=? where id=?";
	  public static final String SEARCH_BY_DID = "select * from doctor where id=?";
	  public static final String DELETE_BY_DID = "delete from doctor where id=?";
	}