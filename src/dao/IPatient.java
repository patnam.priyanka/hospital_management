package dao;
import java.io.IOException;
import java.util.List;
import model.Patient;
public interface IPatient {
  public String addPatient(Patient patient);
  public List<Patient> viewAll();
  public String updatePatient(Patient patient);
  public String searchById(int id);
  public String deleteById(int id);
  
  
  public void addDoctor() throws ClassNotFoundException, IOException;
  public void viewDoctor();
  public void updateDoctor();
  public void searchDoctor();
  public void deleteDoctor();
}