package dao;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import model.Doctor;
import model.Patient;
import util.Query;
public class PatientImpl implements IPatient {
	Scanner sc = new Scanner(System.in);
  PreparedStatement pst;
  ResultSet rs;
  String result;
  int status;
  Doctor doctor = new Doctor();
  @Override
  public String addPatient(Patient patient) {
    try {
      pst = Db.getDb().getConnection().prepareStatement(Query.ADD_PATIENT);
      pst.setInt(1, patient.getId());
      pst.setString(2, patient.getName());
      pst.setInt(3, patient.getBillpaid());
      pst.setLong(4, patient.getMobile());
      pst.setDate(5, patient.getDoj());
      pst.executeUpdate(); // insert your record
      result = patient.getId() + " Added Successfully!!!";
    } catch (ClassNotFoundException | SQLException | IOException e) {
      result = "Duplicate Entry";
    }
    return result;
  }
  @Override
  public List<Patient> viewAll() {    List<Patient> list = new ArrayList<Patient>();
    try {
      pst = Db.getDb().getConnection().prepareStatement(Query.VIEW_ALL);
      rs = pst.executeQuery();
      while (rs.next()) {
    	  Patient patient = new Patient(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getLong(4),
            rs.getDate(5)); // set the value from db
        list.add(patient); // adding 1 by 1 Patient object into the list
      }
    } catch (ClassNotFoundException | SQLException | IOException e) {
      System.err.println("viewAll() raised the exception");
    }
    return list;
  }
  @Override
  public String updatePatient(Patient patient) {
    try {
      pst = Db.getDb().getConnection().prepareStatement(Query.UPDATE);      pst.setString(1, patient.getName());
      pst.setLong(2, patient.getMobile());
      pst.setInt(3, patient.getId());
      status = pst.executeUpdate();
      if (status == 1) {
        result = patient.getId() + " updated successfully!!!";
      } else {
        result = patient.getId() + " record not found";
      }
    } catch (ClassNotFoundException | SQLException | IOException e) {
      System.err.println("updatePatient() raised exception");
    }
    return result;
  }
  @Override
  public String searchById(int id) {
    Patient patient = null;
    try {
      pst = Db.getDb().getConnection().prepareStatement(Query.SEARCH_BY_ID);
      pst.setInt(1, id);
      rs = pst.executeQuery();
      int temp = 0;
      while (rs.next()) { // if you have a record, while block executes
        temp++;
        patient = new Patient(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getLong(4),
            rs.getDate(5));
      }
      if (temp > 0) {
        result = patient.toString();
      } else {
        result = id + " record not found";
      }
    } catch (ClassNotFoundException | SQLException | IOException e) {
      System.err.println("searchById() raised exception");
    }
    return result;
  }
  @Override
  public String deleteById(int id) {
    try {
      pst = Db.getDb().getConnection().prepareStatement(Query.DELETE_BY_ID);
      pst.setInt(1, id);
      status = pst.executeUpdate();
      if (status == 1) {
        result = id + " deleted successfully";
      } else {
        result = id + " record not found";
      }
    } catch (ClassNotFoundException | SQLException | IOException e) {
      System.err.println("deleteById() raised exception");
    }
    return result;
  }
@Override
public void addDoctor() throws ClassNotFoundException, IOException {
	try {
        System.out.println("Enter Doctor Id");
        doctor.setId(sc.nextInt());
        sc.nextLine();
        System.out.println("Enter Doctor Name");
        doctor.setName(sc.nextLine());
        System.out.println("Enter Doctor Salary");
        doctor.setSalary(sc.nextInt());
        System.out.println("Enter Mobile Number");
        doctor.setMobile(sc.nextLong());
        sc.nextLine();
        
    } catch (Exception e) {
        System.err.println("Invalid Data");
    }



   PreparedStatement pst;
    int rs = 0;



   try {
        
        pst = Db.getDb().getConnection().prepareStatement(Query.ADD_DOCTOR);
        pst.setInt(1, doctor.getId());
        pst.setString(2, doctor.getName());
        pst.setInt(3, doctor.getSalary());
        pst.setLong(4, doctor.getMobile());
        
            rs = pst.executeUpdate();
            if (rs == 1) {
                
                System.out.println("Doctor added successfully");
            
        }



   } catch (SQLException e) {
        System.err.println(e);
    }



}	

@Override
public void viewDoctor() {
	try {
        PreparedStatement ps = Db.getDb().getConnection().prepareStatement(Query.VIEW_DOCTOR);
        ResultSet rs = ps.executeQuery();
        System.out.println("Doctors information");
                
        System.out.println("doctorID,Name,salary,Mobile : ");
        while (rs.next()) {



        	System.out.println( rs.getInt(1)+","+ rs.getString(2)+","+ rs.getInt(3)+","+rs.getLong(4));

        }
        



   } catch (Exception e) {
        System.err.println(e);
    }	
}
@Override
public void updateDoctor() {
	try {
		PreparedStatement ps = Db.getDb().getConnection().prepareStatement(Query.UPDATE_DOCTOR);

        System.out.println("Enter Doctor Id to Update");
        ps.setInt(3, sc.nextInt());
        sc.nextLine();
        System.out.println("Enter Doctor Name to Update");
        ps.setString(1, sc.nextLine());
        System.out.println("Enter mobile Number to Update");
        ps.setString(2, sc.nextLine());
        
        int rs = ps.executeUpdate();
        if (rs == 1) {
            
            System.out.println(" Doctor Info Updated");
            
        }
    } catch (Exception e) {
        System.err.println("Failed to Update Doctor Info!");



   }	
}
@Override
public void searchDoctor() {
	try {
        PreparedStatement ps = Db.getDb().getConnection().prepareStatement(Query.SEARCH_BY_DID);
        System.out.println("Enter Id to Search Doctor Info");
        doctor.setId(sc.nextInt());
        ps.setInt(1, doctor.getId());
        ResultSet rs = ps.executeQuery();
        
        System.out.println("DoctorId,name,salary,mobile");
        while (rs.next()) {



            System.out.println( rs.getInt(1)+","+ rs.getString(2)+","+ rs.getInt(3)+","+rs.getLong(4));
        }
        



   } catch (Exception e) {
        System.err.println("Doctor Info not Available for given ID");



   }	
}
@Override
public void deleteDoctor() {
	try {



        PreparedStatement ps = Db.getDb().getConnection().prepareStatement(Query.DELETE_BY_DID);
         System.out.println("Enter Id to Delete Doctor Info");
         ps.setInt(1, sc.nextInt());
         int rs = ps.executeUpdate();
         if (rs == 1) {
             
             System.out.println("Doctor Info Deleted");
             
         }



    } catch (Exception e) {
         System.err.println("Doctor Does not Exists !");



    }	
}
}
