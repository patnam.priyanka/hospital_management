package main;
	import java.text.ParseException;
	import java.text.SimpleDateFormat;
	import java.util.Date;
	import java.util.Scanner;
	import controller.PatientController;
	import model.Patient;
	public class Main { // View
	  static String s[];

	  public static void main(String[] args) {
	    Scanner sc = new Scanner(System.in);
	    try {
	      System.out.println("hospital mangement");
	      int cont = 0;
	      do {
	        System.out.println("1.Add 2.Update(name,mobile,billpaid) 3.Delete 4.Search(id) 5.View All 6.addDoctor 7.viewDoctor 8.updtaeDoctor 9.searchDoctor 10.deleteDoctor");
	        int choice = sc.nextInt();
	        PatientController controller = new PatientController();
	        String result = null;
	        int id = 0;
	        switch (choice) {
	          case 1:
	            sc.nextLine();
	            System.out.println("Enter id,name,billpaid,mobile,doj(dd-MM-yyyy)");
	            s = sc.nextLine().split(",");
	            SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
	            format.setLenient(false);
	            Date date = format.parse(s[4]);
	            result = controller.addPatient(Integer.parseInt(s[0]), s[1], Integer.parseInt(s[2]),
	                Long.parseLong(s[3]), new java.sql.Date(date.getTime()));
	            System.out.println(result);
	            break;
	          case 2:
	            sc.nextLine();
	            System.out.println("Enter id[0],name[1],mobile[2]");
	            s = sc.nextLine().split(",");
	            result = controller.updatePatient(Integer.parseInt(s[0]), s[1], Long.parseLong(s[2]));
	            System.out.println(result);
	            break;
	          case 3:
	            System.out.println("Enter delete id");
	            id = sc.nextInt();
	            result = controller.deleteById(id);
	            System.out.println(result);
	            break;
	          case 4:
	            System.out.println("Enter search id");
	            id = sc.nextInt();
	            result = controller.searchById(id);
	            System.out.format("%-10s%-20s%-10s%-15s%s\n", "ID", "NAME", "BILLPAID", "MOBILE",
	                "DATE OF JOIN");
	            System.out.println(result);
	            break;
	          case 5:
	            System.out.format("%-10s%-20s%-10s%-15s%s\n", "ID", "NAME", "BILLPAID", "MOBILE",
	                "DATE OF JOIN");
	            for (Patient e : controller.viewAll()) {
	              System.out.println(e); // invokes toString() from Patient class
	            }
	            break;
	          case 6:
	          {
                  controller.addDoctor();
                  break;
              }
	          case 7:
	          {
                  controller.viewDoctor();
                  break;
              }
	          case 8:
	          {
                  controller.updateDoctor();
                  break;
              }
	          case 9:
	          {
                  controller.searchDoctor();
                  break;
              }
	          case 10:
	          {
                  controller.deleteDoctor();
                  break;
              }
	          default:
	            System.out.println("Invalid Choice");
	        }
	        System.out.println("Do you want to continue yes-press 1/no-press 0?");
	        cont = sc.nextInt();
	      } while (cont == 1);
	      System.out.println("Program Terminated!!!!");
	    } catch (ParseException e) {
	      System.out.println("Please check your date format");
	    } finally {
	      sc.close();
	    }
	  }
	}
